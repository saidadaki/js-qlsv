function renderDSSV(dssv) {
    var contentHTML = "";
    for (var i=0; i<dssv.length;i++){
        var item = dssv[i];
        var contentTr = `
            <tr>
                <td> ${item.ma} </td>
                <td> ${item.ten} </td>
                <td> ${item.email} </td>
                <td> ${item.tinhDTB} </td>
                <td>
                <button onclick="xoaSV('${item.ma}')" class="btn btn-info">Xoá</button>
                <button onclick="suaSV('${item.ma}')" class="btn btn-primary">Sửa</button>
                </td>
            </tr>`;
        contentHTML += contentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}


function layThongTinTuForm(){
    var ma = document.getElementById("txtMaSV").value;
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var matKhau = document.getElementById("txtPass").value;
    var diemToan = document.getElementById("txtDiemToan").value *1;
    var diemLy = document.getElementById("txtDiemLy").value *1;
    var diemHoa = document.getElementById("txtDiemHoa").value *1;

    var sv = {
        ma: ma,
        ten:ten,
        email: email,
        diemToan: diemToan,
        diemLy: diemLy,
        diemHoa: diemHoa,
        matKhau: matKhau,
        tinhDTB: (this.diemToan + this.diemLy + this.diemHoa)/3,
        
    }
    return sv
}