//lay thong tin
var dssv = [];

/** Get data from LocalStorage when load web */
const DSSV_LOCAL = 'DSSV_LOCAL';
var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null){
    dssv = JSON.parse(jsonData);
    renderDSSV(dssv);
}

function themSV() {
    var sv = layThongTinTuForm();
    
    dssv.push(sv);
    
    //render dssv
    renderDSSV(dssv);
    
    /**==Convert data to store it in LocalStorage */
    let dataJson = JSON.stringify(dssv);
    localStorage.setItem('DSSV_LOCAL', dataJson); /**method to save data to LocalStorage */
    resetForm();
}

function xoaSV(id){
    
    var viTri = -1;
    for(var i=0;i<dssv.length;i++){
        if(dssv[i].ma == id){
            viTri = i;
        }
    }
    if (viTri != -1){
        
        dssv.splice(viTri, 1);
        renderDSSV(dssv);
    }
    // ==*Save data to local storage*== 
    let dataJson = JSON.stringify(dssv);
    localStorage.setItem('DSSV_LOCAL', dataJson)
}

function suaSV(id){
    var viTri = dssv.findIndex(function (item) {
        return item.ma == id;
    });
    console.log(viTri);
    //sau khi da co vi tri thi show thong tin len form
    console.log(dssv[viTri]);
    var sv = dssv[viTri];
    document.getElementById("txtMaSV").value = sv.ma ;
    document.getElementById("txtTenSV").value =sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

function capNhatSV() {
    var sv = layThongTinTuForm();
    console.log(sv);
    var viTri = dssv.findIndex(function (item) {
        return item.ma == sv.ma;
    });
    dssv[viTri] = sv;
    renderDSSV(dssv);
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}

//*** */ Local Storage: luu tru du lieu su dung phuong thuc JSON ***//

